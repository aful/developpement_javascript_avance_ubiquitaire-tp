
Développement JavaScript avancé et ubiquitaire, support de TP
=============================================================

Version 2019-12-31-1 © Aful https://aful.org/ 
distribué sous licence CC-BY-SA 4.0


Installation d'Atom
-------------------

L'éditeur de code recommandé est [Atom](https://atom.io/).
Installez-le et utilisez-le si votre éditeur actuel ne vous aide pas assez.


Installation de Git
-------------------

Il faut disposer de Git pour, plus loin, faire l'installation de NVM.
Il y a différentes façons d'installer NVM, mais l'installation par Git est celle
qu'on préférera.

En tant que `root`, ou avec `sudo` :

```shellsession
# aptitude install git
```


Installation pour compilation
-----------------------------

Certains modules NPM ont du code C/C++ qu'il faut compiler. Sur Debian tous les
paquets nécessaires pour ces compilations sont facilement installés en
installant un paquet unique :

En tant que `root`, ou avec `sudo` :

```shellsession
# aptitude install build-essential
```


Installation de Node.js
-----------------------

On installe Node.js uniquement par le biais de NVM.

:warning: Ne pas installer les paquets `nodejs` et/ou `npm` du système de
paquets de votre distribution GNU-Linux. Cela pourrait provoquer des erreurs
à l'utilisation de `npm` malgré une installation en apparence correcte
(cf. `errno 134`).


```shellsession
$ cd ~/
$ git clone https://github.com/creationix/nvm.git .nvm
$ cd .nvm
$ git checkout v0.33.2
```

### Chargement automatique d'une version de Node.js spécifique

NVM permet notamment de charger une version de Node.js spécifique automatique
lorsqu'un fichier `.nvmrc` est rencontré. Pour cela il faut modifier la
configuration du shell utilisé.

#### Configuration du shell bash

Si le shell utilisé est *bash*, mettre le code suivant dans le fichier
`~/.bashrc` :

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

nvmautouse() {
    if [ -r .nvmrc ]; then
        nvm use
    fi
}

cd() {
    builtin cd "$@"
    nvmautouse
}
```

#### Configuration du shell zsh

Si le shell utilisé est *zsh*, mettre le code suivant dans le fichier
`~/.zshrc` :

```zsh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

autoload -U add-zsh-hook
load-nvmrc() {
  if [[ -r .nvmrc ]]; then
    nvm use
  elif [[ $(nvm version) != $(nvm version default)  ]]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
```

cf. https://gist.github.com/alwayrun/38ca5bb35a22451dec469f04671d30a5


Installation de Node.js
-----------------------

Une fois NVM installé, il est très facile d'installer Node.js, dans toutes les
versions de son choix en fonction des projets :

```shellsession
$ nvm install 12.13.0
$ nvm use 12.13.0
```


Configuration de NPM
--------------------

Le programme `npm` est fourni avec Node.js.
Il est recommandé de faire les actions de configuration suivantes.

### Versions exactes des paquets dans package.json

Cette configuration a pour effet que les versions des paquets enregistrés dans
les fichiers `package.json` seront toujours enregistrées avec des numéros de
version exacts (au lieu de *range* `~`, `^`). Avoir des versions exactes pour
les paquets des projets de l'entreprise permet une reproductibilité exacte
facilement gérable.

```shellsession
$ npm config set save-exact=true
```

### Autocomplétion

Cette configuration a pour effet d'ajouter l'autocomplétion dans le shell.

```shellsession
$ npm completion >> ~/.npm_completion
```

Ajoutez enfin à votre `~/.bashrc` ou votre `~/.zshrc` :

```shell
if [ -r ~/.npm_completion ]; then
    . ~/.npm_completion
fi
```


Outils additionnels de développeurs
-----------------------------------

On peut installer le paquet `jq` pour valider ses fichiers JSON. En effet il
arrive que les fichiers `package.json` soient abimés par les développeurs ou que
les différentes applications qui sont développées dans le TP corrompent le JSON
servant au stockage des données.

Installation :

```shellsession
# aptitude install jq
```

Exemple de vérification d'un fichier `package.json` abimé :

```shellsession
$ cat package.json | jq .
parse error: Expected string key before ':' at line 2, column 7
```

Exemple de vérification d'un fichier `data.json` abimé :

```shellsession
$ cat data.json | jq .
parse error: Expected another key-value pair at line 49, column 5
```


Arborescence des répertoires
----------------------------

Sur votre système, créer les répertoires suivants, un pour chaque projet :

* `JSAU/jsau-apiserver`
* `JSAU/jsau-webserver`
* `JSAU/jsau-webapp`
* `JSAU/jsau-desktop`


Architecture
------------

Voici comment les différentes applications fonctionnent ensemble :

![Communication entre projets](./communication_entre_projets.png)


Ne pas utiliser « sudo » ni « -g »
----------------------------------

Attention lorsque vous utilisez `npm` :

* Ne jamais utiliser `sudo`
* Utiliser `npm install -g` uniquement lorsque nécessaire
  (lorsque le professeur vous le demande)


Création de l'apiserver
-----------------------

```shellsession
$ cd JSAU/jsau-apiserver
$ echo 12.13.0 > .nvmrc
$ npm init
$ npm install express --save
$ mkdir src
```

Créer le fichier `src/index.js`
```javascript
var express = require('express')
var app = express()

// À vous de remplir la suite
```

Et démarrer le serveur Express :
```shellsession
$ node src/index.js
```


Automatisation du démarrage de l'application
--------------------------------------------

Modifier le fichier `package.json` pour avoir :
```json
  "scripts": {
    "start": "node src/index.js"
  }
```

```shellsession
$ npm start
```


Automatisation du rechargement de l'application
-----------------------------------------------

À utiliser pour que le nouveau code serveur de l'application soit pris en
compte.

```shellsession
$ npm install nodemon --save-dev
```

Modifier le fichier `package.json` pour avoir :
```json
  "scripts": {
    "start": "node src/index.js",
    "start:watch": "nodemon"
  }
```

```shellsession
$ npm run start:watch
```


Débogage des routes de l'application
------------------------------------

Installer le paquet `morgan` :
```shellsession
$ npm install morgan --save
```

Insérer ensuite le code suivant dans le code qui instancie le serveur express :
```javascript
const morgan = require('morgan')

app.use(morgan('dev'))
```

Faîtes des requêtes, par exemple avec `curl` qui permet d'utiliser les
différentes méthodes HTTP (GET, POST, PUT, DELETE, etc.) :
```
curl http://localhost:8080/
curl http://localhost:8080/some-bad-route
curl -X POST http://localhost:8080/
curl -X POST http://localhost:8080/some-bad-route
```

Votre serveur une fois démarré affichera les routes utilisées par les requêtes
ainsi que les réponses HTTP (200, 404, etc.) :
```
GET / 200 4.882 ms - 21
GET /some-bad-route 404 4.816 ms - 27
POST / 404 0.719 ms - 14
POST /some-bad-route 404 0.615 ms - 28
```

### Envoi de JSON avec curl

```
curl -X POST -H 'Content-Type: application/json' --data '{"sub":"1234"}' http://localhost:8080/some-route
```


Vérification et amélioration de la qualité du code
--------------------------------------------------

Utilisez [ESLint](https://eslint.org/) avec
[eslint-config-usecases](https://github.com/madarche/eslint-config-usecases)

Modification du `package.json` :
```json
"scripts": {
  "lint": "eslint .",
  "lint:fix": "eslint --fix ."
}
```

Mettre en place les fichiers suivants :
* `.eslintrc.js`
* `.eslintignore`


jsau-webserver
--------------

*jsau-webserver* est quasiment identique à l'*jsau-apiserver*, mais en ajoutant un
moteur de rendu de template HTML. On utilisera le paquet *nunjucks* :

Installer le paquet `nunjucks` :
```shellsession
$ npm install nunjucks --save
```

Cette application doit servir les routes suivantes :

* `/info` en répondant `jsau-webserver-1.0.0`

* `/` en renvoyant une page HTML avec une/des image(s), des CSS et du JavaScript
  qui affiche les N premiers nombres pairs


NPM comme outil de construction
-------------------------------

En plus de permettre de récupérer et d'utiliser du code (cad `npm install`), NPM
est un outil de construction (comparable à Make avec les Makefiles).

Apprenez à utiliser `npm` comme outil de construction :

https://www.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/


Création de modules (1)
-----------------------

Ces modules seront ensuite utilisés dans chacun des projets pour démontrer les
possibilités de réuitilisabilité de code en JavaScript.


Création de modules (2)
-----------------------

Créer un premier fichier `my_shared_code_headless.js` qui sera utilisé par du
code JavaScript serveur.

```javascript
'use strict'

function generateEvenNumbers(max) {
    return ['do it', 'do it', 'do it']
}

module.exports = {
    generateEvenNumbers
}
```


Utilisation du modules code serveur (1)
---------------------------------------

On peut maintenant utiliser le module depuis le fichier `src/index.js`

```javascript
'use strict'

const my_shared_code_headless = require('./my_shared_code_headless')

var express = require('express')
var app = express()
// …

let even_numbers = my_shared_code_headless.generateEvenNumbers(20)
console.log('even_numbers:', even_numbers)
```


Utilisation du modules code serveur (2)
---------------------------------------

Et maintenant à vous de générer une page HTML qui affiche les N premiers nombres
pairs quand on visite la page http://localhost:3000/, toujours en modifiant le
fichiers `src/index.js` :

```javascript
'use strict'

const my_shared_code_headless = require('./my_shared_code_headless')

var express = require('express')
var app = express()
// …

app.get('/, function(req, res) {
    // …
})
```


Création de modules (3)
-----------------------

Créer un second fichier `my_shared_code_ui.js` qui sera utilisé plus tard par du
code JavaScript client.

```javascript
'use strict'

var my_shared_code_headless = require('./my_shared_code_headless')

function writeContent() {
    console.log('TODO: Replace this by actual code')
    console.log('Write the 20 first even numbers,')
    console.log('one per second')
    var numbers = my_shared_code_headless.generateEvenNumbers(20)
}

module.exports = {
    writeContent
}
```


Modules CommonJS pour navigateur web
------------------------------------

Grâce à Browserify on peut utiliser les modules CommonJS avec tous les avantages
que cela représente au niveau du code JavaScript déployé côté client
(navigateur)

80% des 500.000+ paquets NPM disponibles à ce jour fonctionnent côté client

Toutes les différentes possibilités décrites dans
https://github.com/substack/browserify-handbook

### Attention à const et let

`const` et `let` sont des mots clés uniquement disponibles à partir de
ES2015. Il ne faut donc pas les utiliser dans des modules qui vont être importés
côté client.

Remarque : il est en fait possible d'utiliser des `const` et des `let` dans du
code destiné au côté client si ce code est modifié, transpilé d'une version de
JavaScript vers une version plus ancienne, au moment de la browserification. La
transpilation est généralement réalisée avec *Babel*.


Browserification simple (1)
---------------------------

Installation paquet nécessaire :
```shellsession
$ npm install browserify --save-dev
```

Modification du `package.json` :
```json
"scripts": {
  "build": "npm run build:js",
  "build:js": "browserify src/resource/js/main.js > src/resource/js/bundle.js",
  "clean": "echo 'TODO: Implement this script'"
}
```

Pour lancer la construction :
```shellsession
$ npm run build
```

Tous les fichiers générés doivent être facilement supprimables en exécutant le
script `clean` que vous devez écrire :
```shellsession
$ npm run clean
```


Browserification simple (2)
---------------------------

Il faut utiliser le fichier client `main.js` :

```javascript
'use strict'

var my_shared_code_ui = require('../js/my_shared_code_ui')
var body_elem = document.getElementsByTagName('body')[0]

my_shared_code_ui.writeContent(body_elem)
```


Browserification avec minification
----------------------------------

Installation paquet nécessaire :
```shellsession
$ npm install uglify-js --save-dev
```

Modification du `package.json` :
```json
"scripts": {
  "build": "npm run build:js",
  "build:js": "browserify src/resource/js/main.js | uglifyjs --compress > src/resource/js/bundle.min.js",
  "clean": "echo 'TODO: Implement this script'"
}
```

Pour lancer la construction :
```shellsession
$ npm run build
```

Pour supprimer les fichiers générés :
```shellsession
$ npm run clean
```


Processus de construction complet
---------------------------------

Installation paquets nécessaires :
```shellsession
$ npm install less postcss-cli autoprefixer pixrem clean-css clean-css-cli --save-dev
```

Modification du `package.json` :
```json
"scripts": {
  "build": "npm run build:css && npm run build:js",
  "build:css": "mkdir src/resource/css && lessc src/resource/less/main.less | postcss -u autoprefixer -u pixrem -c postcss.json | cleancss > src/resource/css/bundle.min.css",
  "build:js": "browserify src/resource/js/bundle.js | uglifyjs --compress > resource/js/bundle.min.js",
  "clean": "echo 'TODO: Implement this script'"
}
```

Pour lancer la construction :
```shellsession
$ npm run build
```

Pour supprimer les fichiers générés :
```shellsession
$ npm run clean
```


<!--
Utilisation de modules
---------------------------------------

L'application *jsau-webserver* doit utiliser vos modules
`my_shared_code_headless.js` et
`my_shared_code_ui.js`.
-->


jsau-desktop
------------

L'application *jsau-desktop* doit utiliser vos modules
`my_shared_code_headless.js` et
`my_shared_code_ui.js`.

### Travail sur le système de fichiers et les appels asynchrones

L'application *jsau-desktop* doit, dans son interface graphique, avoir un champ
qui permet à l'utilisateur de saisir un chemin sur le système de fichiers. Une
fois le chemin récupéré, l'application récupère toute la structure de
l'arborescence se trouvant sous ce chemin. Par exemple si l'utilisateur choisit
le chemin `/etc/xml`, la structure de l'arborescence à récupérer sera de la
forme suivante :

```
/etc/xml/
├── catalog
├── docbook-xml.xml
├── docbook-xsl.xml
├── resolver
│   └── CatalogManager.properties
├── sgml-data.xml
└── xml-core.xml
```

Cette arborescence est à récupérer en utilisant une méthode *asynchrone* de
l'[API File System de Node.js](https://nodejs.org/api/fs.html).
L'objectif est d'apprendre à utiliser une méthode asynchrone avec l'utilisation
d'un *callback* standard Node.js.

Cette arborescence sera ensuite à envoyer par le réseau à l'application
`jsau-apiserver`.


jsau-webapp
------------

L'application *jsau-desktop* doit utiliser vos modules
`my_shared_code_headless.js` et
`my_shared_code_ui.js`.


jsau-apiserver
--------------

Créez un serveur d'API de type REST, nommé *jsau-apiserver*, qui écoute sur le
port 8081 et que les applications *jsau-desktop* et *jsau-webapp* vont requêter.

Cette application *jsau-apiserver* doit fournir a minima les méthodes HTTP
`GET`, `PUT`, `DELETE`, `POST`
cf. http://www.restapitutorial.com/lessons/httpmethods.html

Et les applications *jsau-desktop* et *jsau-webapp* doivent appeler chacune de
ces méthodes.

Cette application doit servir la route suivante :

* `/info` en répondant `jsau-apiserver-1.0.0`


Factorisation de code avec paquet NPM
=====================================

Mettez les modules `my_shared_code_headless.js` et `my_shared_code_ui.js` dans
un paquet NPM que vous allez créer et qui doit s'appeler `my_shared_code`.

Vous n'allez pas publier ce paquet NPM sur la registry NPM car nous ne voulons
pas la polluer avec des tests.

Mais vous allez publier ce paquet NPM sous forme d'un dépôt Git sur le serveur
Git utilisé pour les TP.
Il est essentiel de mettre le fichier `package.json` de ce paquet NPM à la
racine du projet Git. Si on ne fait pas cela on n'aura pas la possibilité de
faire un npm install via Git de ce package.

Pour installer un paquet NPM sous forme de dépôt Git la syntaxe est la suivante :
```shellsession
$ npm install URL_GIT
```

Avec URL_GIT prenant les formes suivantes :

    git://github.com/user/project.git#commit-ish
    git+ssh://user@hostname:project.git#commit-ish
    git+ssh://user@hostname/project.git#commit-ish
    git+ssh://user@hostname/absolute/path.git#commit-ish
    git+http://user@hostname/project/blah.git#commit-ish
    git+https://user@hostname/project/blah.git#commit-ish

Ce qu'on appelle un *commit-ish* peut être :
* un tag Git
* une branche Git
* l'empreinte d'un commit Git

Exemple d'installation d'un paquet NPM sous forme de dépôt Git :
```shellsession
$ npm install git://github.com/mozilla/node-convict.git#f60a67fb1fd5e87b2d13e5d4a725cb8a0c2753da
```

Vous allez enfin utiliser ce paquet NPM Git dans toutes vos applications à la
place des modules `my_shared_code_headless.js` et `my_shared_code_ui.js`.

Voici comment installer ce paquet NPM Git (votre dépôt n’a pas besoin d’être public) :
```shellsession
$ npm install git+ssh://git@gitlab.somedomain.net/user_name/my_shared_code.git
```


Mise en place d'un reverse proxy avec Nginx
-------------------------------------------

Vous n'aurez pas de problème pour requêter *jsau-apiserver* depuis
*jsau-desktop*. Par contre votre *jsau-webapp* sera empêchée par le navigateur
web de communiquer avec votre *jsau-apiserver* avec le message suivant :

Blocage d’une requête multiorigines (Cross-Origin Request) :

> la politique « Same Origin » ne permet pas de consulter la ressource distante
> située sur http://localhost:8081/.
> Raison : l’en-tête CORS « Access-Control-Allow-Origin » est manquant.

Pour que cela soit possible il faut que *jsau-webapp* et *jsau-apiserver* soient
sur le même URL de base, par exemple sur http://local.test/.

Voici un exemple de fichier de conf `local.test.conf` pour Nginx pour le domaine `local.test` :
```
server {
    server_name local.test;
    listen 80;

    access_log /var/log/nginx/local.test-access.log;
    error_log /var/log/nginx/local.test-error.log;

    location / {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;

      proxy_pass http://127.0.0.1:8080; # TODO: port à adapter pour faire pointer sur webserver
      proxy_redirect off;
    }


    location /api {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;

      proxy_pass http://127.0.0.1:8081; # TODO: port à adapter pour faire pointer sur apiserver
      proxy_redirect off;
    }
}
```

Il faut aussi penser à modifier le fichier `/etc/hosts`


Livrables à fournir pour obtenir une note de TP
-----------------------------------------------

L'objectif de ce TP est de comprendre les différentes interactions possibles
entre les différentes applications qui peuvent être écrites en JavaScript.

Pour évaluer que vous avez atteint l'objectif, vous devez fournir le code source
pour les applications suivantes :

* *jsau-webserver*
* *jsau-apiserver*
* *jsau-webapp*
* *jsau-desktop*

L'évaluation du professeur consiste à construire les applications ci-dessus et
les faire fonctionner. Si les applications se construisent et fonctionnent comme
attendu, alors le TP est réussi.

### Consignes

Chaque élève rend un travail. Les élèves sont invités à s'aider, mais la
réalisation et l'évaluation sont individuelles.

Chaque projet doit avoir un bon niveau de qualité, vérifié en permanence par
l'utilisation de ESLint, et doit contenir les fichiers suivants :
* `.eslintrc.js`
* `.eslintignore`

Les noms de fichiers et de répertoires ne doivent pas contenir d'espaces.

**Les applications sont évaluées et notées en cours uniquement**. Elles doivent
donc être terminées et avoir pu être présentées au professeur
**avant la fin du dernier cours**.
C'est notamment pour éviter les élèves qui ne viendraient pas en cours et qui
voudraient donner un TP simplement recopié sur leurs camarades à la dernière
heure.
Il ne faut donc pas attendre le dernier cours pour présenter votre travail, car
le professeur n'aurait alors pas assez de temps pour valider le travail de tout
le monde. Les applications n'ayant pas pu êre validées par le professeur avant
la fin du dernier cours donneront une note de TP de 0.

Pour les cas exceptionnels, uniquement sur autorisation à obtenir auprès du
professeur, il est possible d'envoyer son travail au professeur sous l'une des
formes suivantes :
* une archive (.tar.gz, .zip, etc.), comprenant le nom de l'élève dans son nom
  de fichier (par exemple `NOM_Prenom-TP-JSAU.tar.gz`)
* un ou plusieurs dépôts Git, dont l'élève donnera le ou les URL au professeur

Ces livrables ne doivent pas contenir de répertoire `node_modules`, ni de
fichier *bundle* ou toute autre information générée par les processus de
construction.

