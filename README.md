
Version 2024-12-01-01 © Aful https://aful.org/
distribué sous licence CC-BY-SA 4.0

[[_TOC_]]


Système de fichiers, avoir et/ou faire de la place, emplacements
----------------------------------------------------------------

Actuellement développer en JavaScript demande de la place sur le système de
fichiers, car il faut disposer d'une version de Node.js et on utilise aussi
globalement de très nombreux paquets NPM en tant que dépendances.

:warning: Donc il faut avoir et/ou faire de la place pour pouvoir accueillir
cette quantité importante de fichiers et bien suivre l'évolution de l'espace
disque disponible pour ne pas remplir le système. Quand l'espace disque
disponible devient trop réduit, le système est généralement ralenti par le
support de stockage. Et quand il n'y a plus d'espace disque disponible, le
système se bloque avec risques de perte de données et impossibilité de
redémarrer. Faire particulièrement attention avec une machine virtuelle ou un
système en dual boot (qui sont généralement de plus petite taille et plus
difficilement redimensionnables qu'un système d'exploitation unique).

:warning: Pour faire tout le TP il faut
**disposer d'un minimum de 2Go d'espace disque disponible**
(200Mb environ pour chaque application + 500Mb environ pour Node.js + marge de
sécurité).

:bulb: Sur macOS il est recommandé de mettre son code dans un dossier `Developer`,
à créer s’il n’existe pas encore.

![Répertoire `Developer` sur macOS](./macos_developer_directory.png)


Installation d'un éditeur de code adapté à JavaScript
-----------------------------------------------------

L’éditeur de code recommandé pour réaliser ce TP est [VSCodium](https://vscodium.com/)
(développé sur le cadriciel [Electron](https://www.electronjs.org/)),
car il est par défaut très adapté pour tout ce qui est développement web et aide à ne pas
commettre d’erreur en repérant facilement les syntaxes mal-formées.

Vous pouvez néanmoins très bien utiliser un autre éditeur de code/EDI comme
*Emacs* ou *VIM* si vous avez déjà de l’expérience avec et que vous les avez
déjà bien configurés pour le développement web.


Installation de Git
-------------------

Il faut disposer de Git pour déployer votre code vers GitLab, et pour plus loin
faire l'installation de NVM. Il y a différentes façons d'installer NVM, mais
l'installation par Git est celle qu'on préférera dans le cadre de ce TP.

En tant que `root`, ou avec `sudo` :

```shell
apt-get install git
```

Quand on utilise Git il est aussi pratique de disposer d'un visualisateur
d'historique des commits et des branches et pour cela on installera `gitk`.

En tant que `root`, ou avec `sudo` :

```shell
apt-get install gitk
```

Pour voir tout l'historique des branches d'un projet git on pourra exécuter la
commande suivante :

```shell
gitk --all
```

Sur macOS l’installation de *git* et de *gitk* pourra se faire avec les
commandes suivantes :

```shell
brew install git
brew install git-gui
```


Installation pour compilation
-----------------------------

Certains modules NPM ont du code C/C++ qu'il faut compiler. Sur Debian tous les
paquets nécessaires pour ces compilations sont facilement installés en
installant un paquet unique :

En tant que `root`, ou avec `sudo` :

```shell
apt-get install build-essential
```


Installation de Node.js
-----------------------

On installe Node.js uniquement par le biais de Node Version Manager (NVM).

:warning: Ne pas installer les paquets `nodejs` et/ou `npm` du système de
paquets de votre distribution GNU-Linux. Cela pourrait provoquer des erreurs
à l'utilisation de `npm` malgré une installation en apparence correcte
(cf. `errno 134`).

### Installation de Node Version Manager (NVM)

```shell
cd ~/
git clone https://github.com/nvm-sh/nvm .nvm
cd .nvm
git checkout v0.39.1
```

### Chargement automatique d'une version spécifique de Node.js

NVM permet notamment de charger une version spécifique de Node.js automatique
lorsqu'un fichier `.nvmrc` est rencontré. Pour cela il faut modifier la
configuration du shell utilisé.

#### Configuration du shell bash

Si le shell utilisé est *bash*, mettre le code suivant dans le fichier
`~/.bashrc` :

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

nvmautouse() {
    if [ -r .nvmrc ]; then
        nvm use
    fi
}

cd() {
    builtin cd "$@"
    nvmautouse
}
```

#### Configuration du shell zsh

Si le shell utilisé est *zsh*, mettre le code suivant dans le fichier
`~/.zshrc` :

```zsh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

autoload -U add-zsh-hook
load-nvmrc() {
  if [[ -r .nvmrc ]]; then
    nvm use
  elif [[ $(nvm version) != $(nvm version default)  ]]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
```

cf. https://gist.github.com/alwayrun/38ca5bb35a22451dec469f04671d30a5

### Installation de versions spécifiques de Node.js

Une fois NVM installé, il est très facile d'installer des versions spécifiques
de Node.js en fonction des projets, par exemple ici la version `20.17` (mais
vous pouvez sélectionner une autre version suffisamment récente et stable) :

```shell
nvm install 20.17
nvm use 20.17
```


Configuration de NPM
--------------------

Le programme `npm` est fourni avec Node.js.
Il est recommandé de faire les actions de configuration suivantes.

### Versions exactes des paquets dans package.json

Cette configuration a pour effet que les versions des paquets enregistrés dans
les fichiers `package.json` seront toujours enregistrées avec des numéros de
version exacts (au lieu de *range* `~`, `^`). Avoir des versions exactes pour
les paquets NPM des projets de l'entreprise permet une reproductibilité exacte
facilement gérable.

```shell
npm config set save-exact=true
```

### Autocomplétion

Cette configuration a pour effet d'ajouter l'autocomplétion dans le shell.

```shell
npm completion >> ~/.npm_completion
```

Ajoutez enfin à votre `~/.bashrc` ou votre `~/.zshrc` :

```shell
if [ -r ~/.npm_completion ]; then
    . ~/.npm_completion
fi
```


Pas de « sudo npm » et attention à « npm install -g »
-----------------------------------------------------

Attention lorsqu’on utilise `npm` :

* Ne jamais faire `sudo npm`
* Ne pas utiliser `npm install -g`, sauf si cela est nécessaire et
  uniquement si le professeur le demande. À la place préférer l’utilisation de
  la commande `npx`, cf. https://docs.npmjs.com/cli/v10/commands/npx


Outils additionnels de développeurs
-----------------------------------

On peut installer le paquet `jq` pour valider ses fichiers JSON. En effet il
arrive que les fichiers `package.json` soient abimés par les développeurs ou que
les différentes applications qui sont développées dans le TP corrompent le JSON
servant au stockage des données.

En tant que `root`, ou avec `sudo` :


```shell
apt-get install jq
```

Exemple de vérification d'un fichier `package.json` abimé :

```shell
cat package.json | jq .
parse error: Expected string key before ':' at line 2, column 7
```

Exemple de vérification d'un fichier `data.json` abimé :

```shell
cat data.json | jq .
parse error: Expected another key-value pair at line 49, column 5
```


Projets GitLab, accès, autoévaluation et évaluation
---------------------------------------------------

Vos projets doivent tous être gérés dans GitLab et avoir une visibility
`Private`.

Le professeur doit pouvoir accéder à chacun de vos projets GitLab. Pour cela
vous devez lui donner le rôle `Reporter` dans chaque projet.

Le professeur effectuera la notation de votre TP sur la base de l’autoévaluation
que vous ferez de votre propre travail : le professeur vérifiera uniquement ce
que vous avez déclaré que vous avez réalisé correctement.

Autoévaluateur : https://aful.gitlab.io/jsau-autoevaluation/

Une fois que vous aurez fini le TP vous devrez exporter votre déclaration
d'autoévaluation dans un fichier JSON que vous commiterez dans un dépôt Git
`jsau-autoevaluation` dédié.

:warning: Un livrable donné au professeur hors projets GitLab sera réputé non
rendu. Un livrable auquel le professeur ne pourra pas avoir accès, par manque du
bon rôle donné au professeur, sera réputé non rendu.


Arborescence des projets Git et GitLab
--------------------------------------

Pour ce TP, il y a différents projets Git à créer et différentes applications à développer.
Chaque application devra être dans un dépôt Git correspondant.
Chaque dépôt Git devra avoir un projet Gitlab correspondant.

Les noms et chemins des projets GitLab, à respecter strictement, sont les suivants :

* `jsau-apiserver`
* `jsau-webserver`
* `jsau-webapp`
* `jsau-desktop`
* `jsau-npmpackage`
* `jsau-autoevaluation`

La manipulation recommandée pour chaque projet est la suivante :

1. Tout d’abord créer le projet dans GitLab.

   :bulb: Vérifier que l’URL du projet dans GitLab correspond bien au nom attendu pour le projet. Si ce n’est pas le cas, le chemin d’un projet peut être modifié par : `Settings` `>` `General` `>` `Advanced` `>` `Change path`

2. Ensuite cloner le projet Git en local sur votre système (idéalement dans un
répertoire `/home/my-user/Documents/JSAU/`).

:warning: Un projet avec un URL non conforme aux instructions ne pourra pas être récupéré par le programme de correction du professeur et sera réputé non rendu.


Ce qui doit être enregistré dans les dépôts Git
-----------------------------------------------

Tout ce qui n’est pas recréable automatiquement *sans perte d’information* doit
être enregistré (commité).

Tout ce qui est recréable automatiquement *sans perte d’information* ne doit pas
être enregistré (commité).

Ainsi les fichiers `package-lock.json` doivent être enregistrés (commités) dans
les dépôts Git car ils ne sont pas recréables automatiquement sans perte
d’information.  En effet ces fichiers sont générés par une action délibérée du
développeur, faite à un instant donné dans le temps, qui définit des arbre de
dépendances potentiellement différents à chaque fois. Enregistrer ces fichiers
est nécessaire pour la reproductibilité du processus de construction.

À l’opposé, les dossiers `node_modules`, les dossiers `dist`, les fichiers
bundles `.js` et `.css`, les fichiers `.map`, les fichiers de *chunk*, etc. ne
doivent pas être enregistrés (commités) dans les dépôts Git. En effet ces
fichiers ne sont porteurs d’aucune information qui ne puisse être recréée.

:warning: Dans un dépôt Git, si la commande suivante affiche des chemins de
fichiers, c’est que ces fichiers ont été commités à un moment ou à un autre et
qu’ils existent toujours dans l’historique du dépôt Git :

```shell
git log --name-status -- 'node_modules' 'dist' '*.min.*' '*.map' '*.bundle.*' '*chunk*' '*coverage*'
```

Il faut supprimer ces fichiers de l’historique des dépôts Git concernés. Il existe
plusieurs manières de procéder, notamment :
* réécrire l’historique des dépôts Git concernés avec la commande
  `git rebase -i`
* purement et simplement supprimer les dépôts Git concernés et les recréer
  (prendre soin de faire des sauvegardes avant pour ne pas perdre de travail en
  cas d’erreur de manipulation)


Architecture des applications
-----------------------------

Voici comment les différentes applications fonctionnent ensemble :

![Communication entre projets](./communication_entre_projets.png)

:warning: Pour la pertinence du TP le stockage des données doit exclusivement
être fait dans un fichier JSON et non dans une base de données, de manière à
travailler sur les problématiques d’asynchronimse pour la manipulation du système de fichiers.


Création des projets Git
------------------------

:hint: Dans les commandes ci-dessous remplacer le scope `my-user` par le nom de votre compte (votre numéro d’étudiant).

La manipulation recommandée est de d’abord créer chacun des projets GitLab, puis de cloner en local sur votre système chacun des projets Git.

Ce qui donnera par exemple pour le premier projet *jsau-apiserver*
(remplacer `20` par la version de Node.js récente de votre choix) :

```shell
cd /home/my-user/Documents/JSAU/
git clone git@gitlab.example.net:my-user/jsau-apiserver.git
cd jsau-apiserver
echo 20.17 > .nvmrc
nvm use
npm init
mkdir src tests
```

:warning: N’ajoutez pas le répertoire `node_modules` ou tout autre fichier généré.


Mise en place de la CI dans GitLab
----------------------------------

### Ajout de scripts npm

Ajouter le script npm `test:ci` en modifiant le fichier `package.json` :
```json
"scripts": {
  "test:ci": "echo 'Bonjour'"
}
```

### Ajout de .gitlab-ci.yml

Créer un fichier `.gitlab-ci.yml` à la racine de chaque projet et à pousser
dans GitLab (remplacer `20` par la version de Node.js récente de votre
choix) :

```yaml
default:
  image: node:20-alpine

# Cache modules in between jobs
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .npm/

tests:
  script:
   - npm ci --cache .npm --prefer-offline
   - npm run test:ci
```

:bulb: Les images Docker *alpine* sont beaucoup plus petites que les images de base. Elles sont suffisantes lorsqu’on a uniquement besoin de Node.js et pas besoin d’outils additionnels Unix. Utiliser les plus petites images possible permet à votre CI/CD d'aller beaucoup plus vite, tout en économisant des ressources ce qui est bon pour la planète :seedling:


Gestion des opérations asynchrones
----------------------------------

* Ne pas utiliser le paquet NPM `bluebird` car Node.js dispose depuis plusieurs
  années maintenant de l'objet
  [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
* Ne pas utiliser la fonction
  [`util.promisify(original)`](https://nodejs.org/api/util.html#util_util_promisify_original)
  de Node.js car Node.js fournit déjà une version sous forme de `Promise` de
  l'API *File system* : [`fs.promises`](https://nodejs.org/api/fs.html#fs_fs_promises_api)



Développement application jsau-apiserver
----------------------------------------

Créer un serveur d'API de type REST, nommé *jsau-apiserver*, que les
applications *jsau-desktop* et *jsau-webapp* requêteront par la suite.

Cette application *jsau-apiserver* doit fournir a minima les méthodes HTTP
`GET`, `POST`, `DELETE`, `PUT` sur les routes concernant les ressources
manipulées
cf. https://www.restapitutorial.com/introduction/httpmethods

Enfin, cette application doit servir en plus la route suivante :

* `/info` en répondant `jsau-apiserver-1.0.0`

Pour déveloper cette application avec le cadriciel Express, prendre comme
exemple le tutoriel https://expressjs.com/en/starter/installing.html

Si vous souhaitez expérimenter l’utiliser de *express-generator* comme présenté
dans https://expressjs.com/en/starter/generator.html, **ne pas utiliser** la
commande avec `npm install -g`, mais utiliser plutôt la commande suivante :

```shell
npx express-generator --view=ejs .
```


Débogage des routes d'une application serveur Express
-----------------------------------------------------

Installer le paquet NPM `morgan` :
```shell
npm install morgan
```

Insérer ensuite le code suivant dans le code qui instancie le serveur express :
```javascript
const morgan = require('morgan')

app.use(morgan('dev'))
```


Test manuel des routes d'une application serveur avec curl
----------------------------------------------------------

Faîtes des requêtes avec `curl` qui permet d'utiliser les
différentes méthodes HTTP (GET, POST, PUT, DELETE, etc.) :
```
curl http://localhost:8080/
curl http://localhost:8080/some-bad-route
curl -X POST http://localhost:8080/
curl -X POST http://localhost:8080/some-bad-route
```

Votre serveur une fois démarré affichera les routes utilisées par les requêtes
ainsi que les réponses HTTP (200, 404, etc.) :
```
GET / 200 4.882 ms - 21
GET /some-bad-route 404 4.816 ms - 27
POST / 404 0.719 ms - 14
POST /some-bad-route 404 0.615 ms - 28
```

### Envoi de JSON avec curl

```
curl -X POST -H 'Content-Type: application/json' --data '{"sub":"1234"}' http://localhost:8080/some-route
```


Automatisation du démarrage de l'application
--------------------------------------------

Modifier le fichier `package.json` pour avoir :
```json
  "scripts": {
    "start": "node src/main.js"
  }
```

```shell
npm start
```


Automatisation du redémarrage de l'application
----------------------------------------------

À utiliser pour que le nouveau code serveur de l'application soit pris en
compte.

Modifier le fichier `package.json` pour avoir :
```json
  "scripts": {
    "start": "node src/main.js",
    "start:watch": "node --watch-path=./src src/main.js""
  }
```

```shell
npm run start:watch
```


Utilisation de variables d’environnement
----------------------------------------

Utiliser une/des variables d’environnement pour définir des emplacements
spécifiques au système sur lequel l’application *jsau-apiserver* s’exécute.

Définir ainsi une variable d’environnement `JSAU_REPOSITORY_FILE_PATH`, ayant pour valeur le chemin
absolu d’un fichier JSON unique, à utiliser par toutes les applications.

```javascript
import { env } from 'node:process';
//const { env } = require('node:process');

console.log('Le stockage aura lieu ici :', env.JSAU_REPOSITORY_FILE_PATH);
```

```shell
export JSAU_REPOSITORY_FILE_PATH="/home/my-user/Documents/JSAU/jsau-data"
```

```
Le stockage aura lieu ici : /home/my-user/Documents/JSAU/jsau-data
```


Développement application jsau-webserver
----------------------------------------

Le *jsau-webserver* est très proche de l’*jsau-apiserver* (qu'on pourra copier
comme base de départ).
Mais dans le *jsau-webserver* on doit remplacer l’envoi et la réception de
données au format `application/json` respectivement par de l’affichage de pages HTML par un
moteur de rendu de template HTML et de l’envoi des interactions du navigateur de
l’utilisatuer par formulaire web au format `application/x-www-form-urlencoded`.

Utiliser le moteur de templating [EJS](https://ejs.co/)

Pour la route `/` cette application doit renvoyer une page HTML avec une/des
image(s), des CSS et du JavaScript *client*.

Les ressources CSS et JavaScript doivent être préparées sous forme de paquets
(*bundles*), avec un *bundler* comme par exemple [Vite](https://vite.dev/) ou
[Webpack](https://webpack.js.org/), pour ne garder que le nécessaire, pour
optimiser et pour compresser.

:warning: Pour la pertinence de cette partie du TP, le JavaScript client doit
servir uniquement à effectuer de petites actions dans le navigateur (par exemple
vérifier que des champs obligatoires sont bien remplis, empêcher que les
formulaires soient soumis plusieurs fois si on clique plusieurs fois sur le
bouton d’envoi). Ce JavaScript client ne doit pas servir à envoyer des données
au format `application/json`.


Utiliser method-override pour pouvoir réaliser la fonctionnalité DELETE depuis jsau-webserver
---------------------------------------------------------------------------------------------

Avec Express, la solution pour pouvoir faire des requêtes HTTP avec des méthodes autres que `GET`
et `POST` est d'utiliser
le paquet NPM [method-override](https://www.npmjs.com/package/method-override).

```javascript
const methodOverride = require('method-override');


// Method override to be able to use PUT in HTML forms
//
// WARNING: to be effective this middleware must be set after the
// bodyParser.urlencoded middleware.
//
// When using req.body, you must fully parse the request body
// before you call methodOverride() in your middleware stack,
// otherwise req.body will not be populated.
app.use(methodOverride((req, res) => {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        const method = req.body._method;
        delete req.body._method;
        return method;
    }
}));
```


Intégration des services (derrière un reverse proxy Nginx)
---------------------------------------------------------

Vous n'aurez pas de problème pour requêter *jsau-apiserver* depuis
*jsau-desktop*. Par contre le navigateur empêchera le *jsau-webapp* de
communiquer avec le *jsau-apiserver* avec le message suivant :

Blocage d’une requête multiorigines (Cross-Origin Request) :

> la politique « Same Origin » ne permet pas de consulter la ressource distante
> située sur http://localhost:8081/.
> Raison : l’en-tête CORS « Access-Control-Allow-Origin » est manquant.

Il vaut éviter d’utiliser le paquet NPM [cors](https://www.npmjs.com/package/cors) qui
enlève ces blocages mais en supprimant les protections contre les attaques CSRF.

Pour que cela soit possible il faut que *jsau-webapp* et *jsau-apiserver* soient
sur le même URL de base, par exemple sur https://local.test/

Les nouvelles API web ne sont actives que sur des connexions HTTPS.

```shell
apt-get install nginx
```

Pour faire du HTTPS il faut disposer d’une clé privée et d’un certificat.

En situation réelle on ferait générer la clé privée et le certificat par
l’*Autorité de Certification* (AC ou CA pour *Certificate Authority* en anglais)
à but non lucratif [Let's Encrypt](https://letsencrypt.org/) (organisme créé par
la fondation Mozilla), par exemple avec l’aide d’un outil comme
[Certbot](https://certbot.eff.org/).

Mais dans le cadre de développements sur un système local on va créer un certificat auto-signé.
Pour cela on va générer la clé privée et le certificat avec le script
`generate_private_key_and_self_signed_cert` suivant :

```shell
#!/bin/sh

# Exit on error
set -o errexit

key_file_path=local.test.key
cert_file_path=local.test.crt

# -nodes option to not protect the private key with a passphrase
# -x509 option tells req to create a self-signed cerificate
openssl req \
        -newkey rsa:2048 -nodes -keyout $key_file_path \
        -x509 -days 1200 -out $cert_file_path

# And then give the following answers to the following questions:
#
# Country Name (2 letter code) [AU]:FR
# State or Province Name (full name) [Some-State]:.
# Locality Name (eg, city) []:Paris
# Organization Name (eg, company) [Internet Widgits Pty Ltd]:Local Test
# Organizational Unit Name (eg, section) []:
# Common Name (e.g. server FQDN or YOUR name) []:*.local.test
# Email Address []:root@localhost

chmod go-rwx $key_file_path
chmod a+r $cert_file_path
```

Note : si le répertoire `/etc/nginx/ssl/private/` n’existe pas sur votre
système, créez-le avec les commandes suivantes :
```shell
su -
mkdir --parent /etc/nginx/ssl/private
chmod go-rwx /etc/nginx/ssl/private
exit
```

Voici un exemple d’un fichier de conf pour Nginx pour le site web du *jsau-webserver*
https://web-1.local.test à mettre dans un fichier `web-1.local.test.conf` :
```
server {
    server_name web-1.local.test;
    listen 80;
    return 301 https://$host$request_uri;
}

server {
    server_name web-1.local.test;
    listen 443 ssl;
    ssl_certificate /etc/nginx/ssl/cert/local.test.crt;
    ssl_certificate_key /etc/nginx/ssl/private/local.test.key;

    access_log /var/log/nginx/web-1.local.test-access.log;
    error_log /var/log/nginx/web-1.local.test-error.log;

    location / {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;

      # TODO: port à modifier pour faire pointer sur le port jsau-webserver
      proxy_pass http://127.0.0.1:8082;
      proxy_redirect off;
    }

}
```

Installer le fichier `web-1.local.test.conf` en tant que `root` ou avec `sudo`
comme suit (créez au besoin les répertoires `/etc/nginx/sites-available/` et
`/etc/nginx/sites-enabled/`) :
```
vim /etc/nginx/sites-available/web-1.local.test.conf
ln -s /etc/nginx/sites-available/web-1.local.test.conf /etc/nginx/sites-enabled/web-1.local.test.conf
nginx -t
systemctl reload nginx
```

Voici un exemple d’un fichier de conf pour Nginx pour le site web du
*jsau-apiserver* et de la *jsau-webapp* https://web-2.local.test à mettre dans
un fichier `web-2.local.test.conf` :
```
server {
    server_name web-2.local.test;
    listen 80;
    return 301 https://$host$request_uri;
}

server {
    server_name web-2.local.test;
    listen 443 ssl;
    ssl_certificate /etc/nginx/ssl/cert/local.test.crt;
    ssl_certificate_key /etc/nginx/ssl/private/local.test.key;

    access_log /var/log/nginx/web-2.local.test-access.log;
    error_log /var/log/nginx/web-2.local.test-error.log;

    root /usr/local/www/jsau-webapp/dist;

    location /api/ { # trailing slash required
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;

      # TODO: port à modifier pour faire pointer sur le port jsau-apiserver
      proxy_pass http://127.0.0.1:8081/; # trailing slash required
      proxy_redirect off;
    }
}
```

Installer le fichier `web-2.local.test.conf` en tant que `root` ou avec `sudo` comme suit :
```
vim /etc/nginx/sites-available/web-2.local.test.conf
ln -s /etc/nginx/sites-available/web-2.local.test.conf /etc/nginx/sites-enabled/web-2.local.test.conf
nginx -t
systemctl reload nginx
systemctl status nginx
```

Il faut aussi ajouter au système la connaissance des noms `web-1.local.test` et
`web-2.local.test` en ajoutant les lignes suivantes dans le fichier `/etc/hosts`
(à la fin du fichier) :
```
127.0.0.1       web-1.local.test
127.0.0.1       web-2.local.test
```

En cas de problème de communication entre Nginx et Node.js, on peut avancer dans
le diagnostique en exécutant, en tant que `root` ou avec `sudo`, la commande
suivante :
```shell
tail -f /var/log/nginx/*
```


Développement application jsau-webapp
-------------------------------------

L’application *jsau-webapp* doit être dans un répertoire qui est accessible à
Nginx en terme de permissions. C’est pourquoi on va créer le répertoire dédié
`/usr/local/www/` avec la commande suivante (remplacer `mon-utilisateur` par le
nom de votre utilisateur Unix) :
```shell
su -
mkdir /usr/local/www/
chown mon-utilisateur /usr/local/www/
chmod a+rx /usr/local/www/
exit
```

Note : une autre possibilité est d’utiliser `~/public_html` à la place de
`/usr/local/www`, mais cela demande que les accès soient soigneusement autorisés.

Création et initialisation du projet *jsau-webapp* :
```shell
cd /usr/local/www/
npm init vue jsau-webapp -- --default
```

Il existe différentes manières d’initialiser un projet Vue.js. Ces manières
évoluent avec les nouveaux développements réalisés par l’équipe de Vue.js. Pour
comprendre la logique de la commande `npm init vue` proposée ici lire les
documents suivants :
* https://docs.npmjs.com/cli/v8/commands/npm-init/
* https://cli.vuejs.org/guide/installation.html
* https://stackoverflow.com/questions/59660391/is-there-any-npx-create-react-app-equivalent-for-vue-cli

Génération des livrables de *jsau-webapp* dans `/usr/local/www/jsau-webapp/dist` :
```shell
cd jsau-webapp
echo 20 > .nvmrc
npm run build
```

Vérifier dans le fichier de configuration Nginx que c'est bien le répertoire
`/usr/local/www/jsau-webapp/dist` (ou `~/public_html/jsau-webapp/dist`) qui est
servi par Nginx.


Développement application jsau-desktop
--------------------------------------

:hint: Dans les commandes ci-dessous remplacer le scope `my-user` par le nom de votre compte (votre numéro d’étudiant).

Pour la création de l’application effectuer les premières actions ci-dessous :

```shell
cd ~/version/JSAU/
git clone git@gitlab.example.net:my-user/jsau-desktop.git
cd jsau-desktop
echo 20.17 > .nvmrc
nvm use
npm init
mkdir src
npm install --save-dev electron
```

Puis poursuivez en suivant les instructions de la page
https://www.electronjs.org/docs/latest/tutorial/quick-start et sans utiliser
*Electron Forge*, car on aurait moins de souplesse dans le cadre de ce TP.


### Démarrage

Le démarrage de l’application se fait comme suit :

```shell
npm start
```

En exécutant `npm start` vous aurez possiblement l’erreur suivante :
```
[FATAL:setuid_sandbox_host.cc] The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that node_modules/electron/dist/chrome-sandbox is owned by root and has mode 4755.
```

Dans ce cas on fait le choix, parce qu'on est sur une version de développement
et parce qu'on exécutera et affichera que des contenus produits par nous (par le
*jsau-apiserver*), de supprimer l'utilisation des couches de sandboxing de
Chromium :
```json
"scripts": {
    "start": "electron --no-sandbox .",
}
```

Une autre solution est d'activer les *Unprivileged User Namespaces*, qui sont
pour l'instant désactivés par défaut sur Debian et d'autres distributions. Et il
faut éviter d'activer cette solution si le système n'est pas récent et bien à
jour en terme de sécurité.

Enfin, quand l'application sera empaquetée (packaged) et livrée sous forme de
paquet (.deb, .rpm), elle ne sera pas exécutée avec cette option `--no-sandbox`.
La bonne solution sera soit de tirer parti des *Unprivileged User Namespaces* si
c'est activé, soit de mettre de positionner un propriétaire et des permissions
spéciales sur le fichier `chrome-sandbox`.

#### Différentes options de démarrage

On peut lancer Electron en fenêtre plein écran.

En spécifiant une option supplémentaire lors de l’appel de la ligne de commande :

```shell
npm start -- --max
```

Ou bien en créant un nouveau script npm :

```json
"scripts": {
    "start-max": "electron --max .",
}
```

On peut lancer Electron en mode debug, avec le panneau *devTools* ouvert.

En spécifiant une option supplémentaire lors de l’appel de la ligne de commande :

```shell
npm start -- --debug
```

Ou bien en créant un nouveau script npm :

```json
"scripts": {
    "start-debug": "electron --debug .",
}
```

On peut combiner plusieurs options :

```shell
npm start -- --debug --max
```


Développement du paquet NPM privé
---------------------------------

### Intérêts

Factoriser le code et les ressources communes entre les applications
*jsau-apiserver*, *jsau-webserver*, *jsau-webapp*, *jsau-desktop*
dans un dépôt Git `jsau-npmpackage`.

Ce dépôt Git sera ensuite « consommé » par ces applications sous forme d’un paquet NPM publié de manière privée.

Ce paquet NPM rendra son code et ses ressources disponibles à l’extérieur en utilisant
l’instruction [`export`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export).

Le code et les ressources à factoriser sont par exemple :
* des fonctions/classes/propriétés, notamment le code assurant le stockage du JSON
  qu'on trouve en doublon dans les applications *jsau-apiserver* et *jsau-webserver*
* des [types d'erreur personnalisés (custom error types)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#custom_error_types)
* des images
* des fontes
* des CSS

La publication privée correspond à de nombreux cas d’utilisation d’organisations/d’entreprises, même si ces structures peuvent publier une partie plus ou moins importante de leur code source sous forme de logiciels libres.

Le TP sera d'autant plus réussi que cette factorisation sera étendue,
car démontrant l’intérêt de développer en JavaScript toutes les applications autour
d'un même ensemble de services.

### Principe : Deploy tokens + Package Registry

On rappelle que comme tous les autres projets GitLab de ce TP, le projet GitLab *jsau-npmpackage* doit avoir une *visibility* `Private`.
Mettre une *visibility* `Public` rend l’exercice en partie inutile puisqu’on n’est plus dans le cas d’une organisation/entreprise qui aurait besoin de garder au moins une partie de son code privée.

Dans GitLab il existe plusieurs concepts relativement similaires avec lesquels on peut réussir à mettre à disposition et consommer un paquet NPM. Ceci peut être fait avec :
* des *Deploy tokens*
* des *Access tokens*

On privilégie l’utilisation des *Deploy tokens* car ces objets sont ceux qui ont les permissions les plus limitées et donc c’est avec eux qu’il y aura le moins de risques de faire des erreurs et de créer des failles.

On va donc utiliser des *Deploy tokens* pour publier, et ensuite récupérer, le paquet NPM `jsau-npmpackage`
dans la *Package Registry* de l’instance GitLab auto-hébergée (self-hosted).

On créera 2 *Deploy tokens*, un pour la publication, un pour la lecture, et on configurera ensuite la *CI/CD* pour utiliser le *Deploy token* créé pour la lecture.

Les documentations de référence sont les suivantes :

* https://docs.npmjs.com/using-private-packages-in-a-ci-cd-workflow/
* https://docs.npmjs.com/cli/v10/using-npm/scope
* https://docs.gitlab.com/ee/user/project/deploy_tokens/
* https://docs.gitlab.com/ee/user/packages/package_registry/index.html
* https://docs.gitlab.com/ee/ci/variables/

### Mise en oeuvre

#### Création du dépôt Git avec un scope particulier

:hint: Dans les commandes ci-dessous remplacer le scope `my-user` par le nom de votre compte (votre numéro d’étudiant).

Contrairement aux fichiers `package.json` des autres projets Git, cette fois on va spécifier un *scope* dans le nom du paquet, de telle sorte que le `name` du paquet NPM sera `@my-user/jsau-npmpackage` et non pas simplement `jsau-npmpackage`.
Cela sert pour pouvoir associer ce paquet à un *NPM registry* particulier.

```shell
cd ~/version/JSAU/
git clone git@gitlab.example.net:my-user/jsau-npmpackage.git
cd jsau-npmpackage
echo 20.17 > .nvmrc
nvm use
npm init --scope=@my-user
mkdir src tests
```

#### Création des Deploy tokens dans GitLab

Dans l’interface utilisateur GitLab du projet Git *jsau-npmpackage*, créer 2 *Deploy tokens* ayant respectivement pour *Name*
`Registry maintainer` et `Registry consumer` et pour *scopes* `read_package_registry`+`write_package_registry` et `read_package_registry`.

![GitLab > Settings > Repository > Deploy tokens](./gitlab-settings-repository-deploy_tokens.png)

#### Ajout d’une variable dans la CI/CD dans GitLab

Dans l’interface utilisateur GitLab des projets Git *jsau-apiserver*, *jsau-webserver*, *jsau-webapp*, *jsau-desktop* ajouter une variable avec les options `Protect variable`, `Protect variable`, la `key` `REGISTRY_CONSUMER_NPM_TOKEN` et ayant pour `value` la valeur du token ``Registry consumer``.

![GitLab > Settings > CI/CD > Variables](./gitlab-settings-ci_cd-variables.png)

#### Configuration npm du NPM registry et authToken associé

Ensuite créer des fichiers `.npmrc` (à commiter) et des fichiers `.env` (à ne pas commiter et à spécifier dans le `.gitignore`), dans les dépôts Git suivants suivant les spécifications ci-dessous :

* Pour *jsau-npmpackage*, mettre dans le fichier `.env` la configuration `REGISTRY_MAINTAINER_NPM_TOKEN="AAAAAAA"`
* Pour *jsau-apiserver*, mettre dans le fichier `.env` la configuration `REGISTRY_CONSUMER_NPM_TOKEN="BBBBBBB"`
* Pour *jsau-webserver*, mettre dans le fichier `.env` la configuration  `REGISTRY_CONSUMER_NPM_TOKEN="BBBBBBB"`
* Pour *jsau-webapp*, mettre dans le fichier `.env` la configuration  `REGISTRY_CONSUMER_NPM_TOKEN="BBBBBBB"`
* Pour *jsau-desktop*, mettre dans le fichier `.env` la configuration  `REGISTRY_CONSUMER_NPM_TOKEN="BBBBBBB"`

#### Publication du paquet NPM privé

Dans le projet Git *jsau-npmpackage* :

1. Créer un fichier `.npmrc` qui va préciser d’utiliser le *Package Registry* de l’instance GitLab auto-hébergée
2. Publier le paquet NPM

```shell
set -a; . ./.env; set +a
npm publish
```

#### Installation du paquet NPM privé

:hint: Dans les commandes ci-dessous remplacer le scope `my-user` par le nom de votre compte (votre numéro d’étudiant).

Dans les projets Git *jsau-apiserver*, *jsau-webserver*, *jsau-webapp*, *jsau-desktop* :

1. Créer un fichier `.npmrc` qui va préciser d’utiliser le *Package Registry* de l’instance GitLab auto-hébergée
2. Publier le paquet NPM

```shell
set -a; . ./.env; set +a
npm i -S @my-user/jsau-npmpackage
```

### Utilisation possible de npm link

Pour éviter de publier à chaque fois, par exemple pendant des périodes de développement, on pourra utiliser [`npm link`](https://docs.npmjs.com/cli/v10/commands/npm-link).

